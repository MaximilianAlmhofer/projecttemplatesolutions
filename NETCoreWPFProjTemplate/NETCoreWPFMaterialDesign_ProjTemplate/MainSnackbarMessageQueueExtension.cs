﻿using System;

namespace NETCoreWPFMaterialDesign_ProjTemplate
{
	[System.Windows.Markup.MarkupExtensionReturnType(typeof(MaterialDesignThemes.Wpf.SnackbarMessageQueue))]
	public class MainSnackbarMessageQueueExtension : System.Windows.Markup.MarkupExtension
	{
		public override object ProvideValue(System.IServiceProvider serviceProvider)
		{
			return new MaterialDesignThemes.Wpf.SnackbarMessageQueue(TimeSpan.FromSeconds(5));
		}
	}
}
