﻿using MaterialDesignColors;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NETCoreWPFMaterialDesign_ProjTemplate
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void MenuItemsListBox_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{

		}

		private void ExpressionDarkMenuItem_Checked(object sender, RoutedEventArgs e)
		{
			App.Current.Resources.BaseTheme = BaseTheme.Dark;
		}

		private void ExpressionLightMenuItem_Checked(object sender, RoutedEventArgs e)
		{
			App.Current.Resources.BaseTheme = BaseTheme.Light;	
		}

		private void MenuPopup_OnClick(object sender, RoutedEventArgs e)
		{
			
		}

		private void ShutdownMenuItem_Click(object sender, RoutedEventArgs e)
		{
			App.Current.Shutdown();
		}

		private void OptionsMenuItem_Click(object sender, RoutedEventArgs e)
		{

		}

		private void LogoutMenuItem_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
