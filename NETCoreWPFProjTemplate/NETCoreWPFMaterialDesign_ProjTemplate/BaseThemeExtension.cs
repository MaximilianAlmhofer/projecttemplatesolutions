﻿namespace NETCoreWPFMaterialDesign_ProjTemplate
{
	[System.Windows.Markup.MarkupExtensionReturnType(typeof(MaterialDesignThemes.Wpf.BaseTheme))]
	public class BaseThemeExtension : System.Windows.Markup.MarkupExtension
	{
		public override object ProvideValue(System.IServiceProvider serviceProvider)
		{
			return App.Current.Resources.BaseTheme!;
		}
	}
}
