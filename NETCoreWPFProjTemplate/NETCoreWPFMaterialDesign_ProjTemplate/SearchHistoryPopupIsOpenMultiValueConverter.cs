﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace NETCoreWPFMaterialDesign_ProjTemplate
{
	public class SearchHistoryPopupIsOpenMultiValueConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
				return Binding.DoNothing;

			bool hasText = (bool)values[0];
			int count = (int)values[1];
			return hasText && count > 0;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
