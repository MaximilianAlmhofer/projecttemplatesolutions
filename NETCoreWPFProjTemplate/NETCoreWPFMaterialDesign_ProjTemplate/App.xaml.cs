﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace NETCoreWPFMaterialDesign_ProjTemplate
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		internal static new App Current => (App)Application.Current;
		public new MaterialDesignThemes.Wpf.BundledTheme Resources => (MaterialDesignThemes.Wpf.BundledTheme)Application.Current.Resources;
	}
}
