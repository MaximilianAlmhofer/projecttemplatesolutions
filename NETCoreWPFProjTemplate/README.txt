VS Projektvorlage für WPF CustomControlsLibrary 
###############################################
Platform		win10-x64
Sdk Version		>= 3.1.402
TargetFramework	netcoreapp3.1

Author			Maximilian Almhofer
Licence			MIT
Repository		https://MaximilianAlmhofer@bitbucket.org/MaximilianAlmhofer/projecttemplatesolutions.git

################################################
1) Initial Commit:

	NETCoreWPFCustomControls:
		Nuget Packages:
			MahApps.Metro.IconPacks.MaterialLight 4.4.0
			MaterialDesignThemes 3.1.3
		C# Classes:
			CustomControl1.cs
		XAML Components:
			Themes/Generic.xaml
			Themes/MaterialDesign.xaml
			Themes/MaterialDesignSource.xaml
			RuntimeColorSchemaDark.xaml

2) Extension 09/16/2020

	NETCoreWPFCustomControls
		C# Classes:
			Common/FrameworkExtensions:	  
				# Useful (and boilerplate) code to traverse the VisualTree searching for some type of child or parent
--