﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using System.Windows.Media;

namespace NETCoreWPFCustomControls.Common
{
	internal static class FrameworkExtensions
	{
		private static readonly BindingFlags InstanceNonPublicGetField = (BindingFlags)1060;

		/// <summary>
		/// Traversiert den VisualTree rekrsiv abwärts, bis ein Element mit dem Typ im angegebenen Typargument gefunden wird.
		/// Es berücksichtigt alle Arten von Ebenenverschachtelung. 
		/// Wenn auf ein Element mit <see cref="UIElementCollection"/> getroffen wird, vgl. <see cref="Panel"/>, erfolgt ein rekursiver Aufruf für jedes der Childelemente.
		/// Wenn auf ein Element mit Content-Eigenschaft gestoßen wird, vgl. <see cref="ContentControl"/>, erfolgt ein rekursiver Aufruf für den Inhalt der Content-Eigenschaft, wenn es sich bei diesem um ein
		/// <see cref="DependencyObject"/> handelt. Ebenso wird mit der Child-Eigenschaft eines <see cref="Popup"/> Controls umgegangen.
		/// Zusätzlich wird noch die Child-Eigenschaft der <see cref="PopupBox"/> behandlet.
		/// Elemente die ausgelassen werden sollen, können in <paramref name="skipTypes"/> angegeben werden.
		/// </summary>
		/// <typeparam name="T">Der gesuchte Typ.</typeparam>
		/// <param name="parent">Das Rootelement bei dem gestartet wird.</param>
		/// <param name="skipTypes">Typen die ausgelassen werden sollen. (Um Performance zu steigern und Stapelüberlauf zu verhindern.)</param>
		public static T FindVisualChild<T>(this DependencyObject parent, params Type[] skipTypes) where T : DependencyObject
		{
			return FindNextVisualChild<T>(parent, skipTypes);
		}

		private static T FindNextVisualChild<T>(this DependencyObject parent, params Type[] skipTypes) where T : DependencyObject
		{
			T foundChild = default;
			skipTypes ??= Array.Empty<Type>();

			#region T ist ein IAddChild mit Children UIElementCollection
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(parent, i);

				if (child != null)
				{
					if (child is T)
					{
						foundChild = (T)child;
						break;
					}
					if (!skipTypes.Contains(child.DependencyObjectType.SystemType))
					{
						foundChild = child.FindNextVisualChild<T>(skipTypes);
						if (foundChild is null)
						{
							continue;
						}

						break;
					}
				}
			}
			#endregion
			#region T ist ein ContentControl
			if (parent is ContentControl)
			{
				if (parent.GetValue(ContentControl.ContentProperty) is DependencyObject content)
				{
					foundChild = content switch
					{
						T _ => (T)content,
						_ => content.FindNextVisualChild<T>(skipTypes)
					};
				}
			}
			#endregion
			#region T ist ein IAddChild mit ChildProperty
			else
			if (parent is IAddChild && parent.GetValue(Popup.ChildProperty) is UIElement element)
			{
				return element.FindVisualChild<T>(skipTypes);
			}
			#endregion
			#region T ist was spezielles (MaterialDesign.Wpf.PopupBox->ControlzEx.PopupEx)
			if (parent is PopupBox popupBox)
			{
				if (popupBox.GetType().GetField("_popup", InstanceNonPublicGetField)?.GetValue(popupBox) is ControlzEx.PopupEx popupEx)
				{
					return popupEx.FindVisualChild<T>(skipTypes);
				}
			}
			#endregion

			return foundChild;
		}

		/// <summary>
		/// Sucht in <see cref="UIElementCollection"/> nach dem gesuchten Typ.
		/// </summary>
		/// <typeparam name="T">Der gesuchte Typ.</typeparam>
		/// <param name="rootObject">Das <see cref="Panel"/>-Abgeleitete Startelement.</param>
		public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject rootObject) where T : DependencyObject
		{
			if (rootObject is null)
			{
				yield break;
			}

			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(rootObject); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(rootObject, i);

				if (child is T typedChild)
				{
					yield return typedChild;
				}

				T childsChild = FindNextVisualChild<T>(child);
				if (childsChild != null)
				{
					yield return childsChild;
				}
			}
		}

		/// <summary>
		/// Führt eine flache Suche entlang des VisualTrees nach oben durch.
		/// </summary>
		/// <typeparam name="T">Der gesuchte Typ.</typeparam>
		/// <param name="child">Das Element bei dem gestartet wird.</param>
		public static T FindParent<T>(this DependencyObject child) where T : DependencyObject
		{
			DependencyObject parent = VisualTreeHelper.GetParent(child);
			do
			{
				if (parent is T matchedParent)
				{
					return matchedParent;
				}

				parent = VisualTreeHelper.GetParent(parent);
			}
			while (parent != null);

			return null;
		}
	}
}
